# Software Development 101

Learn basic software development principles.

## Prerequisites
* NodeJS - https://nodejs.org/en/
* npm package manager (usually comes with NodeJS)
* git

## Getting started
```
git clone git@gitlab.com:greblys/softwaredevelopment101.git
cd softwareDevelopment101
npm install
```

## Run tests
`npm test`