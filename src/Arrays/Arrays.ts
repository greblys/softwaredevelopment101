export function isSorted(x: any[]) {
    let i;
    for (i = 0; i < x.length - 1; i++) {
        if(x[i] > x[i + 1]){
            break;
        }
    }
    return i >= x.length - 1;
}

// find smallest element in remaining unsorted half and swap it with first element of unsorted half.
export function selectionSort(x: any[]) {
    // shallow copy
    const sorted = x.slice();
    // implement me
    return sorted;
}

// pick next element on the right and sort the whole left hand side
export function insertionSort(x: any[]) {
    // shallow copy
    const sorted = x.slice();
    // implement me
    return sorted;
}

// Keep pushing bigger values towards the end of the array. Repeat until array is sorted.
export function bubbleSort(x: any[]) {
    // shallow copy
    const sorted = x.slice();
    // implement me
    return sorted;
}

