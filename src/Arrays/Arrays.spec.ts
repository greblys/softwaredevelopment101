import {selectionSort, insertionSort, bubbleSort, isSorted} from "./Arrays";

describe("Array sorting algorithms:", () => {
    const sortFs = [selectionSort, insertionSort, bubbleSort];

    it("empty array", () => {
        const sorted = sortFs.map(fn => fn([]));
        for (const s of sorted) {
            expect(s).toBeDefined();
            expect(s).toBeInstanceOf(Array);
            if (s) {
                expect(s.length).toEqual(0);
            }
        }
    })

    it("simple array", () => {
        const sorted = sortFs.map(fn => fn([3, 1, 2]));
        for (const s of sorted) {
            expect(s).toBeDefined();
            expect(s).toBeInstanceOf(Array);
            if (s) {
                expect(s.length).toEqual(3);
                expect(s[0]).toEqual(1);
                expect(s[1]).toEqual(2);
                expect(s[2]).toEqual(3);
            }
        }
    })

    it("sorted array", () => {
        const sorted = sortFs.map(fn => fn([1, 2, 3, 74]));
        for (const s of sorted) {
            if (s) {
                expect(s.length).toEqual(4);
                expect(s[0]).toEqual(1);
                expect(s[1]).toEqual(2);
                expect(s[2]).toEqual(3);
                expect(s[3]).toEqual(74);
            }
        }
    })

    it("reverse array", () => {
        const sorted = sortFs.map(fn => fn([74, 3, 2, 1]));
        for (const s of sorted) {
            if (s) {
                expect(s.length).toEqual(4);
                expect(s[0]).toEqual(1);
                expect(s[1]).toEqual(2);
                expect(s[2]).toEqual(3);
                expect(s[3]).toEqual(74);
            }
        }
    })

    it("one element", () => {
        const sorted = sortFs.map(fn => fn([1]));
        for (const s of sorted) {
            expect(s).toBeDefined();
            if (s) {
                expect(s.length).toEqual(1);
                expect(s[0]).toEqual(1);
            }
        }
    })

    it("sort strings", () => {
        const sorted = sortFs.map(fn => fn(["abc", "aaaaaaa", "george", "beta", "betsy"]));
        for (const s of sorted) {
            expect(s).toBeDefined();
            expect(s).toBeInstanceOf(Array);
            if (s) {
                expect(s.length).toEqual(5);
                expect(s[0]).toEqual("aaaaaaa");
                expect(s[1]).toEqual("abc");
                expect(s[2]).toEqual("beta");
                expect(s[3]).toEqual("betsy");
                expect(s[4]).toEqual("george");
            }
        }
    })

    it("random array", () => {
        const random: any[] = [];
        const factor = 1000000;
        let i;
        for (i = 0; i < Math.random() * factor; i++) {
            random.push(Math.floor(Math.random() * factor));
        }
        const sorted = sortFs.map(fn => fn(random));
        for (const s of sorted) {
            expect(s.length).toEqual(i);
            expect(isSorted(s)).toBeTrue();
        }
    })
});